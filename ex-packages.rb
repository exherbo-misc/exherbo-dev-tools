#!/usr/bin/env ruby
# Copyright 2015 Nikolay Orliuk <virkony@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require 'Paludis'
require 'set'
require 'pathname'
require 'time'

module SimpleSpec
	attr_accessor :env
	def installed_packages(root = '/')
		env[
			Paludis::Selection::AllVersionsUnsorted.new(
				Paludis::FilteredGenerator.new(
					Paludis::Generator::Matches.new(
						self,
						nil,
						[]
					),
					Paludis::Filter::InstalledAtRoot.new( root )
				)
			)
		]
	end
end

class SimplePaludis
	def initialize
		@env = Paludis::PaludisEnvironment.new
	end

	def parse_spec(spec_str, options = [])
		spec = Paludis.parse_user_package_dep_spec(spec_str, @env, options)
		spec.extend(SimpleSpec)
		spec.env = @env
		spec
	end

	def installed_packages(root = '/')
		@env[
			Paludis::Selection::AllVersionsUnsorted.new(
				Paludis::FilteredGenerator.new(
					Paludis::Generator::All.new(),
					Paludis::Filter::InstalledAtRoot.new( root )
				)
			)
		]
	end

	def installed_by_spec(spec_str, root = '/')
		parse_spec(spec_str, [:allow_wildcards, :no_disambiguation]).installed_packages(root)
	end

	def installed_package(spec_str, root = '/')
		pkgids = parse_spec(spec_str).installed_packages(root)
		raise ArgumentError, "Ambiguous spec #{spec_str.inspect} (got #{pkgids.count} matches)" unless pkgids.one?
		pkgids.first
	end
end

class Paludis::PackageID
	def slotted_package_spec
		uniquely_identifying_spec.package.to_s + uniquely_identifying_spec.slot_requirement.to_s
	end

	def each_filepath(&block)
		seq = Enumerator.new do |enum|
			contents and contents.each do |file|
				if file.instance_of?(Paludis::ContentsFileEntry)
					file.each_metadata do |meta|
						if meta.instance_of?(Paludis::MetadataFSPathKey)
							path = Pathname.new(meta.parse_value)
							enum.yield(path)
						end
					end
				end
			end
		end
		block_given? ? seq.each(&block) : seq
	end
end

class CLI
	# TODO: bump commander to 4.3.2
	# include Commander::Methods

	def report_packages(pkgids)
		pkgids.lazy.each { |pkgid| puts pkgid.slotted_package_spec }
	end

	def do_matches(spec_str)
		pkgids = @paludis.installed_by_spec(spec_str, @root)
		report_packages(pkgids)
	end

	def do_owners(paths)
		pkgids = @paludis.installed_packages(@root).lazy.select { |pkgid|
			pkgid.each_filepath.any? { |f| paths.any? { |p| f.fnmatch?(p) } }
		}
		report_packages(pkgids)
	end

	def do_older(pkgid, match = nil)
		boundary = pkgid.installed_time_key.parse_value

		pkgids = match.nil? ? @paludis.installed_packages(@root)
		                    : @paludis.installed_by_spec(match, @root)

		pkgids = pkgids.lazy.select { |pkgid|
			t = pkgid.installed_time_key
			t and t.parse_value < boundary
		}
		report_packages(pkgids)
	end

	def initialize(root = '/')
		@root = root
		@paludis = SimplePaludis.new
	end

	def run
		program :version, '0.1.0'
		program :description, 'Exherbo packages lookup by some obscure criteria with output suitable for installing.'

		command :matches do |c|
			c.syntax = 'matches <spec>'
			c.description = 'Shows package IDs matching given spec'
			c.action do |args, options|
				raise ArgumentError, 'Exactly one argument expected' unless args.one?
				do_matches(args[0])
			end
		end

		command :owners do |c|
			c.syntax = 'owners <glob patterns...>'
			c.description = 'Shows package IDs owning a given file patterns. ' +
				'Somehow similar to "cave owner" but supports multiple patterns ' +
				'and tries to match whole path.'
			c.example 'Find when we can replace /usr/bin and /bin with symlinks to /usr/host/bin',
				'owners /{,usr/}{,s}bin/"*"'
			c.example 'Find which of multilib packages should be cross-installed',
				'owners /{,usr/}lib32/"*"'
			c.example 'Find which packages still waits for migration to multiarch',
				'owners /{,usr/}{lib{32,64},{,s}bin}/"*"'
			c.action do |args, options|
				raise ArgumentError, 'At least one argument expected' if args.empty?
				do_owners(args)
			end
		end

		command :older do |c|
			c.syntax = 'older [--matcho <spec>] <spec>'
			c.description = 'Find all packages that installed earlier than specified one.'
			c.option '--match SPEC', String, 'Restrict only to packages matching SPEC'
			c.example 'Find a set of haskell libs to be rebuilt with a new GHC',
				'older --match "dev-haskell/*" dev-lang/ghc'
			c.example 'Report ::arbor packages for rebuild with a freshly installed gcc:4.9',
				'older --match "*/*::arbor->" gcc:4.9'
			c.action do |args, options|
				raise ArgumentError, 'Exactly one argument expected' unless args.one?
				do_older(@paludis.installed_package(args[0]), options.match)
			end
		end

		command :multiarch do |c|
			c.syntax = 'multiarch [options...]'
			c.description = 'Shows package IDs owning a folders belonging to old pre-cross Exherbo'
			c.option '--native', TrueClass, 'Do not report packages with files in lib32'
			c.action do |args, options|
				paths = []
				paths << '/bin/*' << '/usr/bin/*' <<
				         '/sbin/*' << '/usr/sbin/*' <<
				         '/lib64/*' << '/usr/lib64/*'
				paths << '/lib32/*' << '/usr/lib32/*' unless options.native
				do_owners(paths)
			end
		end

		#run!
	end
end

if $0 == __FILE__
	require 'commander/import'
	CLI.new.run
end
