#!/bin/bash
#
# Paludis hook to check common mistakes in using different paths.
# To install either copy or symlink to folder
#     /etc/paludis/hooks/ebuild_install_post/
#
# Copyright 2015 Nikolay Orliuk <virkony@gmail.com>
# Distributed under the terms of the GNU General Public License v2
#
# Some checks based on Gerrit comments from:
#     Wulf C. Krueger <philantrop@exherbo.org>
#

qa-exherbo-paths-alternatives() {
    # ensure there is no broken symlinks for alternatvies

    # Caution: inherits local variable "failed" from qa-exherbo-paths()

    local ALTERNATIVES_DIR=${ALTERNATIVES_DIR:-/etc/env.d/alternatives}

    [[ -d "${IMAGE}${ALTERNATIVES_DIR}" ]] || return 0

    # find all symlinks that dereference to symlink (broken ones)
    local broken_symlinks=($(find "${IMAGE}${ALTERNATIVES_DIR}" -type l -xtype l))
    local broken_symlink target alt_provider
    for broken_symlink in "${broken_symlinks[@]}"; do
        target=$(readlink -f "${broken_symlink}")
        if [[ "$(readlink -f $(type -fp readlink))" != */greadlink ]];then
            # non-gnu readlink does not work well with this
            target=${broken_symlink}
        fi

        # make it relative to root (${IMAGE})
        target=/${target#${IMAGE}}

        if [[ ${PN} == eclectic-* ]]; then
            # `eclectic-*` packages are special in the sense that they provide
            # alternatives for files provided by other packages. In this case
            # we check if the target of the symlink is present in ${ROOT}
            [[ -e ${ROOT}${target} ]] && continue
        else
            # check if there is any alternatives that can resolve this
            for alt_provider in "${IMAGE}${ALTERNATIVES_DIR}"/*/*/; do
                echo "Checking alternatives provider: ${alt_provider}"
                [[ -L ${alt_provider}${target} ]] && break
                alt_provider= # no match
            done

            [[ -n ${alt_provider} ]] && continue # there is a provider for it
        fi

        eerror "  ${broken_symlink#${IMAGE}} => ${target} (broken symlink)"
        failed=1 # inherited from scope of qa-exherbo-paths()
    done
}

qa-exherbo-paths() {
    local host=$(exhost --target)
    local failed=0 # shared with all called functions

    pushd "${IMAGE}"  > /dev/null
    local folder

    # deprecated pre-multiarch lib folders or kind of mixup
    for folder in {,usr/}lib{,32,64} usr/${host}/lib{32,64}; do
        if [[ -d "${folder}" ]]; then
            eerror "Use of deprecated lib folder /${folder}"
            failed=1
        fi
    done

    # invalid doc folder
    for folder in usr/share/doc/*; do
        [[ -d "${folder}" ]] || continue # only expanded globs are welcome
        case  "${folder}" in
        usr/share/doc/HTML) ;; # User facing docs for packages by KDE
        usr/share/doc/perl) ;; # dedicated folder for perl packages
        usr/share/doc/${PNVR}) ;;
        usr/share/doc/qt5) ;; # API docs of Qt5 based packages (assistant)
        usr/share/doc/qt6) ;; # API docs of Qt6 based packages (assistant)
        *)
            eerror "Invalid doc folder: /${folder} (should use \${PNVR} = ${PNVR})"
            ;;
        esac
    done

    if [[ -d usr/${host}/share ]]; then
        eerror "Invalid /usr/${host}/share is a wrong place for platform-independent folder"
    fi

    # musl specific checks
    if [[ ${host} == *-musl* ]];then
        for file in usr/share/locale/locale.alias usr/${host}/lib/charset.alias;do
            [[ -f "${file}" ]] || continue
            if [[ "${CATEGORY}/${PN}" != "sys-devel/gettext" ]];then
                eerror "${file} should not be installed by anything but gettext." \
                       "You need to disable the package's included gettext with gt_cv_func_gnugettext{1,2}_libc=yes." \
                       "But if it really can't use musl's gettext, just remove the files in src_install."
                failed=1
            fi
        done
    fi

    if [[ -d usr/share/pkgconfig ]]; then
        ewarn "Only arch-independent .pc files should go into /usr/share/pkgconfig." \
              "Most of the times you want /usr/${host}/lib/pkgconfig (Please double-check)"
    fi

    qa-exherbo-paths-alternatives

    # some defaults unapplicable for Exherbo
    if [[ -d etc/bash_completion.d ]]; then
        eerror "Use dobashcompletion to install bash completions but do not" \
               "install them directly into /etc/bash_completion.d folder"
    fi

    popd > /dev/null

    [[ ${failed} == 0 ]] || die "Exherbo paths validation failed"
}

qa-exherbo-paths

# ex: et:sw=4
